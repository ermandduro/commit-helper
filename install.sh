#!/usr/bin/env bash

# Foreground colors
FG_WHITE="\033[97m"
FG_GREEN="\033[32m"
FG_BLACK="\033[30m"

# Background colors
BG_RED="\033[41m"
BG_YELLOW="\033[43m"

# Use colors, but only if connected to a terminal, and that terminal
# supports them.
if [ -t 1 ]; then
    ERROR="${BG_RED}${FG_WHITE}"
    INFO="${BG_YELLOW}${FG_BLACK}"
    NC="\033[0m" # reset
else
    ERROR=""
    INFO=""
    NC=""
fi

GIT=$(which git)

# Path to your oh-my-zsh installation.
PROJECT_DIRECTORY=$HOME/.commit_helper

_install () {

    if [[ ! -d "${PROJECT_DIRECTORY}" ]];then
        echo "${INFO}Cloning Commit helper...${NC}"
        hash git >/dev/null 2>&1 || {
            echo "${ERROR}Error: git is not installed${NC}"
            exit 1
        }

        $GIT clone --depth=1 -b pip-installer https://gitlab.com/marius-rizac/commit-helper.git "${PROJECT_DIRECTORY}"
        WHAT='Installed'
    else
        cd "${PROJECT_DIRECTORY}" && git reset --hard && git pull --rebase -v
        WHAT='Updated'
    fi

    cd "${PROJECT_DIRECTORY}"
    make preinstall

    echo -e "----- Successfully ${WHAT} -----\n"
}

_install
#!/usr/bin/env python

from setuptools import find_packages, setup

files = ["src/commit.py"]

setup(
    name='commit',
    description='Append ticket number in commit message',
    version='0.1',
    author='Bogdan Rizac',
    author_email='mariusbogdan83@gmail.com',
    scripts=['src/commit.py'],
    entry_points={'console_scripts': [
        'commit = commit:run_commit',
    ]},
    packages=find_packages(),
)

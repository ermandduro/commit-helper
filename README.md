# Commit message helper

## Installation

```bash
pip install git+https://gitlab.com/marius-rizac/commit-helper.git
```

## Description

The script will look for branches named like:

```bash
{bugfix|feature|hotfix}/{project-code}-{ticket-number}-{ticket-description}

# Examples
bugfix/AB-1234-ticket-description
feature/AB-1234-ticket-description
hotfix/AB-1234-ticket-description
```

To make a commit you can type:

```bash
commit this is my commit message
```

And the resulted commit will be:

```bash
AB-1234 commit this is my commit message
```


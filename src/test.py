import unittest
from commit import Commit
from subprocess import Popen, PIPE


class TestCommit(unittest.TestCase):

    def test_on_wrong_branch(self):
        cmt = Commit('.')
        cmt.make_commit(['hello', 'world'])

        self.assertIn('does not contain', cmt.get_output())

    def test_empty_commit_message(self):
        cmt = Commit('.')
        cmt.make_commit([])

        self.assertEqual('You must provide a message', cmt.get_output())

    def test_successful_commit(self):
        commands = [
            'git checkout -b feature/AB-123-my-branch',
            'ls -la',
            'git status',
            'git add demo.txt',
            'git status',
            'git log --graph --oneline --decorate',
        ]

        for cmd in commands:
            cli = Popen(cmd, shell=True, universal_newlines=True, stdout=PIPE, stderr=PIPE)
            cli.communicate()

        cmt = Commit('.')
        cmt.make_commit(['hello', 'world'])

        self.assertFalse(cmt.is_failure)

        process = Popen(['git', 'log', '--graph', '--oneline', '--decorate'], stdout=PIPE, stderr=PIPE)
        output, errors = process.communicate()

        if 'hello' not in str(output):
            self.fail('Committed message not found')


if __name__ == '__main__':
    unittest.main()

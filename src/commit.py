#!/usr/bin/env python

import sys
import re
import os
from subprocess import Popen, PIPE


class CommitError:
    def __init__(self, error_text):
        self.error = error_text

    def __iter__(self):
        return self

    def next(self):
        return self.__next__()

    def __next__(self):
        return None, self.error


class Commit:
    def __init__(self, current_working_directory):
        self.cwd = current_working_directory
        self.is_failure = False
        self.error_message = ''
        self.git_commit = 'git rev-parse --abbrev-ref HEAD'
        self.ticket_regex = '([A-Z]{2,3})\-([\d]+)'
        self.branch_name_format = '<feature|bugfix|hotfix>/LN-1000-branch-name'

    def run_command(self, command):
        process = Popen(command.split(' '), cwd=self.cwd, stdout=PIPE, stderr=PIPE)

        return process.communicate()

    def get_branch_name(self):
        stdout, stderr = self.run_command(self.git_commit)

        return stdout

    def get_ticket_number(self, branch):
        output = re.search(self.ticket_regex, str(branch))
        if output is None:
            return False

        return output.group(0)

    def make_commit(self, arguments):
        branch = self.get_branch_name()
        ticket_number = self.get_ticket_number(branch)

        if len(arguments) < 1:
            self.is_failure = True
            self.error_message = 'You must provide a message'

            return None, self.error_message

        commit_message = ' '.join(arguments)
        if ticket_number is False:
            self.is_failure = True
            self.error_message = '{0}\nPlease create a branch in format {1}'.format(
                'Looks like your current branch does not contain a valid ticket number',
                self.branch_name_format
            )

            return None, self.error_message
        else:
            process = Popen('git commit -m "%s %s"' % (ticket_number, commit_message), shell=True, universal_newlines=True, cwd=self.cwd, stdout=PIPE, stderr=PIPE)

            return process.communicate()

    def get_output(self):
        return self.error_message


def run_commit():
    cmt = Commit(os.getcwd())
    parameters = sys.argv[1:]

    if len(parameters) > 0 and parameters[0] == '-h':
        help_message = """
    Lumas commit helper.

        Append ticket number from branch name to the commit message

        Quotes are not required

    Usage:

        commit this is my commit message -> will append the ticket number from the branch to the commit message

    Example:

        You are on branch
            feature/AB-100-new-requirements

        You type:
            git add . 
            commit created new service class
            git push

        Commit message will be:
            AB-100 created new service class

        """
        print(help_message)
    else:
        cmt.make_commit(parameters)
        if cmt.is_failure:
            color_red = "\033[0;31m"
            color_reset = "\033[0m"
            print('%s%s%s' % (color_red, cmt.get_output(), color_reset))